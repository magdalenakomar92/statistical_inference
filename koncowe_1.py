# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st

# Znajdźmy prawdopodobieństwo, że Z < −2,47.
# Na dwa sposoby: raz z użyciem wygenerowanych zmiennych z rozkładu normalnego, drugi raz z użyciem dystrybuanty norm.cdf.

N = 10000
z_crit = -2.47

# symulacja

Z = st.norm.rvs(loc = 0, scale = 1, size = N) # wylosuj N liczb z rozkladu Z = N(0,1)
p = float(np.sum(Z < z_crit))/N

# dystrybuanta

p_cdf = st.norm.cdf(z_crit)

print 'symulowane p:', p
print 'p z dystrybuanty:', p_cdf

# odp: p = 0,0068
