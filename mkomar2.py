# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 09:40:39 2018

@author: mk374226
"""

import scipy.stats as st
import numpy as np


# moc 150 KM,
# a odchylenie standardowe wynosi 13 KM. .



p = st.norm.cdf(-2) 
print ('prawdopodobieĹstwo odczytane z dystrybuanty rozkĹadu normalnego: %(0).4f'%{'0':p})

p_alt = st.norm.cdf(160, loc = 150, scale = 13/10.)
print ("p_alt:", p_alt)


# symulacja

mu = 150

sig = 13
N_prob = 100 #testujemy 100 razy; takie jest moje zalozenie, bo w tresci zadania nie ma podanej liczny testów

m_kryt = 160
N_rep=int(1e4)

srednia=np.zeros(N_rep)
for i in range(N_rep):
    seria=st.norm.rvs(loc=mu, scale=sig, size=100) 
    srednia[i]=np.mean(seria)
print ('prawdopodobieĹstwo uzyskane z symulacji: %(0).4f' %{'0':p})