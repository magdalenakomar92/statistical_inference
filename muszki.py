# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st
import pylab as py

# Tesotowanie hipotez statystycznych
# mutacje muszek owocowych

def randsample(x,ile):
	ind = st.randint.rvs(0,len(x),size = ile)
	y = x[ind]
	return y

# z dystrybuanty

p_parametryczne = st.binom.cdf(6,20,0.5) # binom - rozkład dwumianowy (próby Bernoulliego); p = 0.5 bo proporcja 1:1
print p_parametryczne

# z symulacji

N_rep = 10000
wektor_muszek = np.concatenate(( np.zeros(10), np.ones(10) )) # sklejanie wektorów

a = np.zeros(N_rep)
for i in range(N_rep):
    a[i] = np.sum(randsample(wektor_muszek, 20))

p = np.sum(a<=6)/float(N_rep)

print p

# oszacowalismy prawdopodobienstwo p
