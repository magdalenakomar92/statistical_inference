# -*- coding: utf-8 -*-

from scipy.misc import derivative

# function to calculate partial derivative (for error propagation):
def partial_derivative(func, var=0, point=[]):
    args = point[:]
    def wraps(x):
        args[var] = x
        return func(*args)
    return derivative(wraps, point[var], dx = 1e-6)
