# -*- coding: utf-8 -*-

import numpy as np
import pylab as py
import scipy.stats as st

# Koncentracja zanieczyszczeń w półprzewodniku używanym do produkcji procesorów podlega rozkładowi normalnemu
# o średniej 127 cząsteczek na milion i odchyleniu standardowemu 22.
# Półprzewodnik może zostać użyty jedynie gdy koncentracja zanieczyszczeń spada poniżej 150 cząstek na milion.
# Jaka proporcja półprzewodników nadaje się do użycia?
# Prawdopodobieństwo obliczyć korzystając z dystrybuanty rozkładu normalnego oraz z symulacji.

mu = 127 # średnia
sig = 22 # odchylenie standardowe
m_kryt = 150


# dystrybuanta rozkładu normalnego:

p = st.norm.cdf(m_kryt, loc = mu, scale = sig)

print 'prawdopodobieństwo odczytane z dystrybuanty rozkładu normalnego: %(0).4f' %{'0':p}\

# symulacja:

N = 10000
x = st.norm.rvs(loc = mu, scale = sig, size = N) # wylosuj N liczb z rozkladu normalnego
p_sym = float(np.sum(x < m_kryt))/N

print 'prawdopodobienstwo z symulacji:', p_sym

# Odp.: p = 0,852
