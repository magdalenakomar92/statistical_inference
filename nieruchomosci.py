# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st

# Czy ceny gruntu wzrosły?
# czyli test jednostronny

# Hipoteza zerowa: ceny nie uległy zmianie

# Dane:

mu_0 = 49
a = 0.01
N = 18
x = 38
s = 14

# Odchylenie standardowe od średniej:

std_mu=s/np.sqrt(N)

# wartość statystyki:
 
t=(mu_0-x)/std_mu

# Wartość krytyczna rozkładu:

t_kryt_prawy = st.t.ppf( 1-a, N-1)
 
print 'obliczona wartość statystyki t: ', t
print 'wartość krytyczna t: %(tp).2f '%{'tp':t_kryt_prawy}



p =  1-st.t.cdf(t, N-1)
print 'Prawdopodobieństwo zaobserwowania bardziej ekstremalnych wartości t: %(p).4f'%{'p':p}
