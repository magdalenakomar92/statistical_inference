# -*- coding: utf-8 -*-

def dec2bin(n, l):
    '''konwertuje dziesiętną liczbę całkowitą na tablicę 
    przedstawiającą reprezentację binarną tej liczby
    n liczba do konwersji
    l długość reprezentacji binarnej 
    zwracana jest binarna reprezentacja liczby 
    skonwertowana do tablicy logicznej (0->False, 1-> True)
    '''
    b = np.zeros(l, dtype = bool)
    if n < 0:  raise ValueError, "must be a positive integer"
    i = 1
    while n > 0:
        b[l-i] = bool( n % 2 ) 
        n = n >> 1
        i += 1
    return b

def hist_z_markerem(x, N_bins, marker):
	'''Rysuje histogram wartości w tablicy x, używając N_bins binów. 
	Na lewo od wartości wskazanej przez marker dorysowywany jest prostokąt'''
	
	r = np.max(x) - np.min(x)	
	szer_binu = float(r)/N_bins

	#konstruujemy biny
        # robimy biny od markera co szerokość binu aż do x minimalnego
	biny_na_lewo = np.arange( marker, np.min(x), -szer_binu)
        # odwracamy kolejność tej sekwencji żeby była rosnąca
	biny_na_lewo = biny_na_lewo[-1::-1] 
         # robimy biny od markera co szerokość binu aż do x maksymalnego
	biny_na_prawo = np.arange(marker,np.max(x), szer_binu)
        # sklejamy oba zakresy binów
	biny = np.concatenate((biny_na_lewo, biny_na_prawo))
	(n,xx,patch) = py.hist(x,bins = biny )
	py.fill([np.min(xx), np.min(xx), marker, marker] , [0, np.max(n), np.max(n), 0] ,'r' ,alpha = 0.2)
