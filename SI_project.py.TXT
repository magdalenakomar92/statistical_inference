a = 0.05
b = 0.1
a = 0.05
b = 0.1
r = 30
c = 1
d = 1
e = 1

def randSample(x):
    n = len(x)
    A = np.random.randint(n, size = n)
    r = x[A]
    return r
    
def funkcjaBledu(x, y, wspolczynniki, err):
    W = np.poly1d(wspolczynniki)
    y_fit = W(x)
    residuum = y - y_fit
    residuumWazone = residuum / err
    return (residuumWazone, y_fit)

def funkcjaZSzumem(x, wspolczynniki):
    W = np.poly1d(wspolczynniki)
    y = W(x)
    niepewnosciY = np.zeros(r)
    for i in range(r):
        niepewnosciY[i] = a * y[i]
        y[i] += (( random.random() * 2 ) - 1) * b * y[i]
    return (y, niepewnosciY)

tabX = np.linspace(-2, 16, r)
tabY = np.zeros(r)
wspolczynniki = (c, d, e)
STOPIEN_WIELOMIANU = len(wspolczynniki) - 1

tabY, niepewnosciY = funkcjaZSzumem(tabX, wspolczynniki)

wyliczoneWspolczynniki = np.polyfit(tabX, tabY, deg = STOPIEN_WIELOMIANU, w = 1 / niepewnosciY)

residua , y_fit = funkcjaBledu(tabX, tabY, wyliczoneWspolczynniki, niepewnosciY)

W, p = st.shapiro(residua)
print('Test normalności residuów: p = %.3f'%(p))
chi2 = np.sum(residua**2)

liczbaStopniSwobody = r- len(wyliczoneWspolczynniki)
if chi2 < liczbaStopniSwobody:
    p_chi2 = st.chi2.cdf(chi2, liczbaStopniSwobody)
else:
    p_chi2 = st.chi2.sf(chi2, liczbaStopniSwobody) 
print('chi2 = %.2f, p_chi2 = %.3f' %(chi2, p_chi2))
 