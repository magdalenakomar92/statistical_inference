# -*- coding: utf-8 -*-
 
import scipy.stats as st

# Biuro podróży chce oszacować średnią ilość pieniędzy wydaną na wakacje przez osoby korzystające z jego usług.
# Ludzie przeprowadzający analizę chcieliby móc oszacować średni koszt wakacji z dokładnością do 200 zł na poziomie ufności 95%.
# Z poprzednich doświadczeń tego biura podróży wynika, że odchylenie standardowe w populacji wynosi sigma = 400 zł.
# Jaka będzie minimalna wielkość próby?


# dla rozkładu standardowego Z


Z = st.norm.ppf(0.025)

sigma = 400.0

dokladnosc = 200.0

print Z**2 * sigma**2 / dokladnosc**2

# Odp: n = 15,366 więc wielkość próby wynosi 16 (zaokrąglamy w górę).
