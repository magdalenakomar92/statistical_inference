# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 09:26:23 2017

@author: mk374226
"""



import scipy.stats as st

p = 1/2 
n = 30 #liczba pacjentow przed p. Kingą
k = 21 

p_bin = 1-st.binom.cdf(k-1,n,p) 

print('Prawdopodobienstwo, że pacjentce uda zapisać się na wyrwanie zęba to [%]: ', p_bin*100) 