import numpy as np
import pylab as py
import scipy.stats as st

### ROZKLAD JEDNOSTAJNY = ROZKLAD JEDNORODNY

### generowanie danych = symulacja eksperymentu

dane = st.uniform.rvs(size=5000) # losuje 5000 danych z rozkladu jednostajnego
py.hist(dane, normed=True) # generuje unormowany histogram z danych

srednia_gen = np.mean(dane) # wartosc oczekiwana estymowana z danych ze wzoru suma(x)/n
print("estymowana wartosc oczekiwana:",srednia_gen)
var_gen = (np.std(dane, ddof=1))**2 # wariancja estymowana z danych ze wzoru suma(x-srednia)**2/(n-1)
print("wariancja z wylosowanych danych", var_gen)

### wartosci teoretyczne

os_x = np.arange(-1,2,0.1) # os x do liczenia krzywej teoretycznej
os_y = st.uniform.pdf(os_x) # oblicza wartosci teoretyczne rozkladu jednostajnego dla liczb z osi x
py.plot(os_x, os_y, "r--", linewidth=3)

srednia_teor = st.uniform.mean() # podaje srednia dla rozkladu jednostajnego ze wzoru na moment zwykly, E(x)
print("teoretyczna srednia:", srednia_teor)
var_teor = st.uniform.var() # podaje wariancje dla rozkladu jednostajnego ze wzoru na drugi moment centralny, E((x-srednia)**2)
print("teoretyczna wariancja:", var_teor)

py.show() # pokazuje histogram i krzywa
