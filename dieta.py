# -*- coding: utf-8 -*-
 
import scipy.stats as st
import pylab as py
import numpy as np

x = np.array((15.43, 16.92, 14.43, 12.94, 15.92, 17.42, 18.91, 16.92, 14.93, 14.49, 15.92, 15.43)) # zmierzone przyrosty masy

srednia = np.mean(x)

alfa = 0.05

odch_stand = np.std(x, ddof=1)

N = len(x)
l_stopni_swobody = N -1

print 'Z rozkładu normalnego: '
print st.t.ppf([alfa / 2, (1 - alfa) / 2], l_stopni_swobody, loc = srednia, scale = odch_stand/((N)**0.5))




# bootsratp:

def randsample(x,ile): # zwraca wektor z losowo wybranymi liczbami
	ind = st.randint.rvs(0,len(x),size = ile)
	y = x[ind]
	return y

Nboot = 100000

M = np.zeros(Nboot)

for i in xrange(Nboot):
    X = randsample(x,N)
    M[i] = np.mean(X)

lo = st.scoreatpercentile(M, per = (alfa/2) * 100) # funkcja wyznaczająca percentyle
hi = st.scoreatpercentile(M, per = (1-(alfa/2)) * 100)

print 'przedzial ufnosci: %(lo).2f - %(hi).2f\n'%{'lo':lo,'hi':hi} # .2f == dwa miejsca po przecinku




# histogram:

szer_binu = (hi - lo) / 10
biny = np.arange(lo-10*szer_binu, hi+10*szer_binu, szer_binu)
(n,x,patch) = py.hist(M,bins = biny)
py.plot([lo, lo] , [0, np.max(n)] ,'r' )
py.plot([hi,hi],[0, np.max(n)],'r')
py.show()
