# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st
import pylab as py

# WAŻNE

# p = ?; test dwustronny (bo "czy się zmieniło")

# hipoteza zerowa: ciężar bagażu się nie zmienił

# Dane:

mu_0 = 12
a = 0.05
N = 144
x = 14.6
s = 7.8

# Odchylenie standardowe od średniej:

std_mu=s/np.sqrt(N)

# wartość statystyki:
 
t=(mu_0-x)/std_mu

# Wartości krytyczne rozkładu:

t_kryt_lewy = st.t.ppf(a/2,N-1)
t_kryt_prawy = st.t.ppf( 1-a/2, N-1)
 
print 'obliczona wartość statystyki t: ', t
print 'wartości krytyczne t: %(tl).2f %(tp).2f '%{'tl':t_kryt_lewy, 'tp':t_kryt_prawy}



p =  (st.t.cdf(-np.abs(t),N-1)) + (1-st.t.cdf(np.abs(t), N-1))  # sumujemy po obu ogonach  bo test jest dwustronny; dodaje się
print 'Prawdopodobieństwo zaobserwowania bardziej ekstremalnych wartości t: %(p).4f'%{'p':p}




# wykres:

os_t = np.arange(-5, 5, 0.1)
py.plot(os_t, st.t.pdf(os_t,N-1)) #rysujemy funkcję gęstości prawdopodobieństwa t o N-1 st. swobody

#cieniujemy lewy obszary pod funkcją gęstości prawdopodobieństwa odpowiadający obliczonemu t_kryt_lewy
os_t2 = np.arange(-5, t_kryt_lewy, 0.1)
py.fill_between(os_t2,st.t.pdf(os_t2,N-1)) 
#cieniujemy prawy obszary pod funkcją gęstości prawdopodobieństwa odpowiadający obliczonemu t_kryt_prawy
os_t3 = np.arange(t_kryt_prawy, 5,  0.1)
py.fill_between(os_t3,st.t.pdf(os_t3,N-1)) 

# zaznaczamy obliczoną wartość statystyki:
py.plot(t,0, 'ro')
py.show()


# Wyliczone t leży poza obszarem akceptacji hipotezy zerowej, zatem odrzucamy hipotezę zerową.
