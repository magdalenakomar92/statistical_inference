# -*- coding: utf-8 -*-
import numpy as np
import pylab as py

N=5
#l_stopni_swobody = N - 1
N_liczb=10000

m=np.zeros(N_liczb)
y_idealne=np.zeros(N_liczb)
s_idealne=np.zeros(N_liczb)
y_est=np.zeros(N_liczb)
s_est=np.zeros(N_liczb)

for i in range(N_liczb):
    x=np.random.randn(N)+2 # random.randn - dane z rozkladu normalnego, sigma = 1, a mu bedzie 2
    m[i]=np.mean(x) # srednia z x
    s_idealne[i]=1/np.sqrt(N) # niep.sredniej_teoria = sigma/sqrt(N) = 1/sqrt(N)
    y_idealne[i]=(m[i]-2)/(s_idealne[i]) # teoretyczny rozklad Z = ( srednia - 2 ) / niep.sredniej_teoria
    s_est[i]=np.std(x,ddof=1)/np.sqrt(N) # niep.sredniej_pomiar = std/sqrt(N)
    y_est[i]=(m[i]-2)/(s_est[i]) # zmierzony rozklad t = ( srednia - 2 ) / niep.sredniej_pomiar

py.subplot(311)
py.plot(m)
py.title(u'średnia')

py.subplot(323)
py.plot(s_idealne)
py.title('odchylenie standardowe idealne')

py.subplot(324)
py.plot(s_est)
py.title('odchylenie standardowe estymowane')

py.subplot(325)
py.hist(y_idealne,30,(-4,4))
py.title(u'rozkład ilorazu dla wersji idealnej')

py.subplot(326)
py.hist(y_est,30,(-4,4))
py.title(u'rozkład ilorazu dla wersji estymowanej')

py.show()
