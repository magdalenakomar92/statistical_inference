# -*- coding: utf-8 -*-
 
import scipy.stats as st
import pylab as py
import numpy as np

dane = np.array((11.4,13.4,13.5,13.8,13.9,14.4,14.5,15,15.1,15.8,16,16.3,16.5,16.9,17,17.2,17.5,19.0))

# mediana

mediana = np.median(dane)

print 'Mediana: ', mediana

alfa = 0.05

N = len(dane)

# z bootstarpu (powtarzanie ze zwracaniem i za każdym razem obliczamy medianę)

def randsample(x,ile):
	ind = st.randint.rvs(0,len(x),size = ile)
	y = x[ind]
	return y

Nboot = 100000 # ilość powtórzeń

M = np.zeros(Nboot)

for i in xrange(Nboot):
    X = randsample(dane,N)
    M[i] = np.median(X)

lo = st.scoreatpercentile(M, per = alfa/2*100)
hi = st.scoreatpercentile(M, per = (1-alfa/2)*100)

print 'przedzial ufnosci: %(lo).2f - %(hi).2f\n'%{'lo':lo,'hi':hi}
