#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 11:47:19 2019

@author: mk374226
"""
import scipy.stats as st
#import pylab as py
import numpy as np

alfa = 0.1
p = 0.37
N = 26
k = 19
p_bino = 1-st.binom.cdf(k-1,N,p) 
print('Prawdopodobieństwo zdania egzaminu w tej grupie: %(p).4f'%{'p':p_bino})
if p_bino > 0.1:
    print('Hipoteza zerowa o braku wplywu nauki na wyniki egzaminu - przyjete: ')
else:
    print('Hipoteze zerowa o braku wplywu nauki na wyniki egzaminu - odrzucono:  ')


def randsample(x,ile):
	ind = st.randint.rvs(0,len(x),size = ile)
	y = x[ind]
	return y

studenci = np.ones(100)
studenci[37:] = 0           

Nboot = 1000
wynik = np.zeros(Nboot)

for i in range(Nboot):
    proba = randsample(wynik, N)
    wynik[i] = np.sum(proba)
 
pboot = float(np.sum(wynik>=k))/Nboot
print('Prawdopodobieństwo zdania egzaminu w tej grupie: %(p).4f'%{'p':pboot})
if pboot > 0.1:
    print('Hipoteza zerowa o braku wplywu nauki na wyniki egzaminu - przyjete:')
else:
    print('Hipoteza zerowa o braku wplywu nauki na wyniki egzaminu - odrzucono:')
