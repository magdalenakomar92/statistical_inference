#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 12:24:32 2019

@author: mk374226
"""



import scipy.stats as st
#import pylab as py
#import numpy as np

n = 30

s2 = 32.8

alfa = 0.05

chi_kryt_d = st.chi2.ppf(1-alfa/2, n-1) # zakres i liczba stopni swobody
chi_kryt_g = st.chi2.ppf(alfa/2, n-1)

ci_d = (n-1)*s2/chi_kryt_d
ci_g = (n-1)*s2/chi_kryt_g

print (ci_d, ci_g)
