# -*- coding: utf-8 -*-
import scipy.stats as st
import numpy as np

def randsample(x, N):
	'''zwraca wektor z losowo wybranymi elementami wektora x. Losowanie odbywa się z powtórzeniami''' 
	n=len(x)
	ind = np.random.randint(n, size = N)
	y = x[ind]
	return y


# dane:
wys = 135.0
nis = 470.0
N = 605
wys_zaw = 10.0
nis_zaw = 21.0
N_zaw = wys_zaw + nis_zaw

# uwaga: grupy różnią się ilością
# hipoteza zerowa: wysoki poziom cholesterolu nie zwiększa ryzyka zawału serca

# H0: proporcja zawałowców w obu grupach taka sama:
p_za = N_zaw/N
# H1: proporcja jest różna, test jednostronny (wysoki cholesterol ZWIĘKSZA ryzyko)

roznica = wys_zaw/wys - nis_zaw/nis # taką przymujemy statystykę - wymyślamy względnie wiarygodną


# parametrycznie
# (zakładamy rozkład dwumianowy)

p_binom = 0.0
for k_wys in range(int(wys)+1):
 	p_wys = st.binom.pmf(k_wys,wys,p_za) # prawdopodobieństwo uzyskania k_wys zawałów grupie o liczebności wys
	for k_nis in range(int(nis)+1): 
		if k_wys/wys - k_nis/nis>=roznica: # czy liczebności k_wys i k_nis dają większą bądź równą różnice proporcji?
			p_nis = st.binom.pmf(k_nis,nis,p_za) # prawdopodobieństwo uzyskania k_nis zawałów w grupie o niskim cholesterolu
		 	p_binom += p_wys*p_nis # zdarzenia są niezależne

print p_binom

# symulacja (resampling):

N_rep = 100000
# świat zgodny z H0:
H0 = np.concatenate((np.zeros(N-N_zaw), np.ones(N_zaw))) # 0 - brak zawału, 1 - zawał

wyn = np.zeros(N_rep)
for i in range(N_rep):
	wys_zaw_r = np.sum(randsample(H0,wys))
	nis_zaw_r = np.sum(randsample(H0,nis))
	wyn[i] = wys_zaw_r/wys - nis_zaw_r/nis
p = np.sum(wyn>=roznica)/float(N_rep)
print p
