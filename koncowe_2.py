# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st

# Znaleźć prawdopodobieństwo P(|Z| < 2).
# Czyli Z < 2 i Z > -2

p = st.norm.cdf(2) - st.norm.cdf(-2)

print p

# Odp: p = 0,9545.
