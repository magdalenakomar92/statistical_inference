# -*- coding: utf-8 -*-

import pylab as py
import numpy as np
import scipy.stats as st

# Z oszacowań agencji wynika, że średnio 2 z 3 reklam spotyka się z pozytywnym odzewem.
# Akcja marketingowa obejmuje 12 reklam.
# Niech X oznacza liczbę reklam skutecznych. Czy X podlega rozkładowi dwumianowemu?
# Jakie jest prawdopodobieństwo, że 10 reklam będzie skutecznych?

p = 2./3
n = 12

# ze wzoru

p_wz = st.binom.pmf(10, n, p) # pmf - funkcja prawdopodobienstwa
print p_wz

# symulacja

N_powt = 10000
k = np.zeros(N_powt)

for i in xrange(N_powt):
    r = np.random.random(size = n) # los liczb 0-1
    k[i] = np.sum(r<=p)

p_sym = float(np.sum(k==10))/N_powt #!
print p_sym
