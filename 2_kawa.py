# -*- coding: utf-8 -*-
 
import scipy.stats as st
import pylab as py
import numpy as np

n = 30

s2 = 18.54

alfa = 0.05

chi_kryt_d = st.chi2.ppf(1-alfa/2, n-1) # zakres i liczba stopni swobody
chi_kryt_g = st.chi2.ppf(alfa/2, n-1) # zakres i liczba stopni swobody

ci_d = (n-1)*s2/chi_kryt_d # ze wzoru na chi2
ci_g = (n-1)*s2/chi_kryt_g

print ci_d, ci_g
