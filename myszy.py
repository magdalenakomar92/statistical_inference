# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st
import pylab as py

def randsample(x, N):
	'''zwraca wektor z losowo wybranymi elementami wektora x. Losowanie odbywa sie z powtorzeniami''' 
	n=len(x)
	ind = np.random.randint(n, size = N)
	y = x[ind]
	return y

# WAŻANE!


# Treść:
'''Przedzial ufnosci dla roznicy dwóch srednich

Mamy 7 myszy, którym podano srodek, który mial poprawic ich przezywalnosc 
po operacji oraz 9 myszy kontrolnych, którym owego srodka nie podano. 
Myszy traktowane specjalnie przezyly
94 38 23 197 99 16 141 dni
a myszy traktowane standardowo:
52 10 40 104 51 27 146 30 46 dni
srednia roznica wynosi 30.63 dni dluzej dla myszy traktowanych po nowemu. 
Pytanie, na które chcielibysmy znac odpowiedz to: Czy nowy srodek faktycznie 
poprawia przezywalnosc.

Skonstruujmy przedzial ufnosci 95% dla sredniej roznicy w przezywalnosci.

Uwaga: przy tym problemie kazda z grup traktujemy jako reprezentantów bardzo 
duzych populacji. '''

# dane:

m_srodek = 7 # mysz ze środkiem
m_kontrol = 9 # mysz kontrolnych

zycie_srodek = np.array((94, 38, 23, 197, 99, 16, 141)) # długość życia myszy z zaaplikowanym środkiem
zycie_kontrol = np.array((52, 10, 40, 104, 51, 27, 146, 30, 46)) # długość życia myszy kontrolnych

srednia_roznica = np.mean(zycie_srodek) - np.mean(zycie_kontrol)
print 'Średnia różnica: ', srednia_roznica , 'dni'

    # Czy nowy środek faktycznie poprawia przeżywalność?



# parametrycznie (wzory dla dwóch próbek danych):

f = m_srodek + m_kontrol - 2 # liczba stopni swobody
v_srodek = np.var(zycie_srodek) # wariancja
v_kontrol = np.var(zycie_kontrol) # wariacja

sig = np.sqrt( ((m_srodek*v_srodek + m_kontrol*v_kontrol)) / f * (m_srodek + m_kontrol) / float(m_srodek * m_kontrol) ) # ze wzoru na niepewność różnicy średnich

t_2_5 = st.t.ppf(0.025,f) # odwrotność dystrybuanty
t_97_5 = st.t.ppf(0.975,f) # odwrotność dystrybuanty

print 'Przedział ufności dla różnicy średnich przy założeniu normalności %(d).2f %(g).2f'%{'d':sig * t_2_5 + srednia_roznica,'g':sig * t_97_5 + srednia_roznica}



# nieparametrycznie (symulacja):

POP = np.concatenate((zycie_srodek, zycie_kontrol)) # POP zawiera świat zgodny z H0 = lek nic nie daje
alfa = 0.05 # poziom ufności

N_rep = 10000
r = np.zeros(N_rep)
for i in xrange(N_rep):
	g_srodek = randsample(POP, m_srodek)
	g_kontrol = randsample(POP, m_kontrol)
	r[i] = np.mean(g_srodek) - np.mean(g_kontrol) # <- tu jest różnica średnich

ci_d = st.scoreatpercentile(r, per = alfa/2 * 100)
ci_g = st.scoreatpercentile(r, per = (1 - alfa/2) * 100)

print 'przedział ufności nieparametrycznie: %(d).2f %(g).2f'%{'m':np.mean(r),'d':ci_d, 'g':ci_g}


# Odp.: Zaobserwowana różnica mogła powstać przez przypadek, lek nie działa.
