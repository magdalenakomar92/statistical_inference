# -*- coding: utf-8 -*-

import pylab as py
import numpy as np

def gen_CTG(N, N_liczb):
    k = np.zeros(N_liczb)
    for i in xrange(N_liczb):
        r = np.random.random(size=N)
        k[i] = np.sum(r)-N/2.0 # przesunięcie wykresu do punktu x=0
    return k

# kod testujący

N_liczb = 10000
N = 12

for n in range(1, N+1):
    x = gen_CTG(n, N_liczb)
    py.subplot(4,3,n)
    bins = np.arange(-4,4,0.1)
    py.hist(x,bins)
    miu = np.mean(x)
    sig = np.var(x)
    
    py.title(n)
    py.title(round(miu, 5), loc='left')
    py.title(round(sig, 5), loc='right')
    
py.tight_layout()
py.show()
