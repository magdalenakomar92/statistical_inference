# -*- coding: utf-8 -*-
 
import scipy.stats as st
import pylab as py
import numpy as np

dane = np.array((32, 33, 28, 37, 29, 30, 25, 27, 39, 40, 26, 26, 27, 30, 25, 30, 31, 29, 24, 36, 25, 37, 37, 20, 22, 35, 23, 28, 30, 36, 40, 41))
n = len(dane)

# zakładamy rozkład t

srednia = np.mean(dane)
odch_stand = np.std(dane, ddof=1) # polecenie na odchylenie standardowe od średniej

print st.t.ppf([0.005, 0.995], l_stopni_swobody, loc = srednia, scale = odch_stand/(n**0.5))
