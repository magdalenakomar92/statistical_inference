# -*- coding: utf-8 -*-
import scipy.stats as st

def randsample(x,ile):
	ind = st.randint.rvs(0,len(x),size = ile)
	y = x[ind]
	return y

# zmienna urodzenia byka/krowy podlega rozkładowi dwumianowemu:
p = 100.0/206
N = 10
k = 9
p_bino = 1 - st.binom.cdf(k-1,N,p)
print p_bino

# Odp.: p=0.008, odrzucamy H0 (o braku efektow)
